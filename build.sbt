name := "CS441_HW5_SparkPageRank"

version := "0.1"

scalaVersion := "2.11.0"

scalacOptions += "-target:jvm-1.7"
javacOptions in (Compile, compile) ++= Seq("-source", "1.7", "-target", "1.7", "-g:lines")

assemblyMergeStrategy in assembly := { case PathList("META-INF", xs @ _*) => MergeStrategy.discard

case x => MergeStrategy.first }

libraryDependencies ++=  Seq(
  "org.apache.spark" % "spark-core_2.11" % "2.4.0",
  "org.apache.spark" %% "spark-sql" % "2.4.0",
  "com.typesafe" % "config" % "1.2.1",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
  "ch.qos.logback" % "logback-classic" % "1.1.7",
  "org.scala-lang.modules" %% "scala-xml" % "1.1.1"
)

libraryDependencies += "com.databricks" % "spark-xml_2.11" % "0.5.0"
libraryDependencies += "com.novocode" % "junit-interface" % "0.8" % "test->default"
testOptions += Tests.Argument(TestFrameworks.JUnit, "-v")