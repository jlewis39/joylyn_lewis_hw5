package com.cs441.hw5

//Unit Tests for Spark - Page Rank program
//Submitted by: Joylyn Lewis

//Import required libraries
import org.junit.Assert._
import org.junit.Test
import scala.io.Source
import org.apache.spark.sql.SparkSession
import com.typesafe.config._

class SparkPageRank_Test extends java.io.Serializable{

  val config = ConfigFactory.load("application.conf")                                                  //Load the application's configuration from the classpath resource basename

  //Modeled a small sample based exactly on the example provided in https://community.hortonworks.com/idea/102753/tutorial-101-pagerank-example-in-spark-20-understa.html
  //As per the example provided in the link, page rank values of the 4 nodes are:
  //(D,1.3705281840649928), (B,0.4613200524321036), (C,0.7323900229505396), (A,1.4357617405523626)
  val inputData = Seq(("A", "D"),("B", "A"),("C", "B"),("C", "A"),("D", "C"),("D", "A"))

  //Create a Spark Session object to interact with the Spark functionality
  val spark = SparkSession.builder.master(config.getString("Spark.master")).appName(config.getString("Spark.appName")) getOrCreate


  //Parallelize the collection accumulator value so that the elements form a RDD that can be operated on in parallel
  val lines = spark.sparkContext.parallelize(inputData)

  //Apply the groupByKey command on the key/value pair RDD to produce the links RDD, which is also a key/value pair
  val links = lines.distinct().groupByKey().cache()

  //create the ranks <key,one> RDD from the links RDD. Here, initial seed number 1.0 is assigned
  var ranks = links.mapValues(v => config.getString("Spark.initialRank").toDouble)

  //Obtain number of iterations to be performed from input command line argument else default value will be used
  val iterations = config.getString("Spark.defaultIterations").toInt

  1 to iterations foreach {_ =>
  {
    //calculate the contributions of the node, contrib = rank/size
    val contribs = links.join(ranks).values.flatMap { case (nodes, rank) =>
      val size = nodes.size
      nodes.map(node => (node, rank / size))
    }
    //Set each new rank for each node based on the formula: (1-d) + d * contrib, where d is the damping factor
    ranks = contribs.reduceByKey(_ + _).mapValues(config.getString("Spark.randomFactor").toDouble + config.getString("Spark.dampingFactor").toDouble * _)
  }
  }

  //To get the page rank output results, fetch the entire RDD
  val output = ranks.collect()

  //Unit Test 1: Test difference between page rank values of A and B lie within range
  @Test
  def comparePageRank_AB : Unit= {
    val diffAB = (output.find(_._1 == "A").get._2) - (output.find(_._1 == "B").get._2)
    assertTrue(0.96 < diffAB && diffAB < 0.98)
  }

  //Unit Test 2: Test difference between page rank values of C and D lie within range
  @Test
  def comparePageRank_CD : Unit= {
    val diffCD = (output.find(_._1 == "D").get._2) - (output.find(_._1 == "C").get._2)
    assertTrue(0.63 < diffCD && diffCD < 0.65)
  }

  //Unit Test 3: Check whether node A has the highest page rank value based on the provided example
  @Test
  def checkHighestPageRank : Unit = {
    val sortedList = output.sortBy(_._2)
    assertTrue(sortedList.last._1 == "A")
  }

  //Unit Test 4: Check that the page ranks calculated are for four nodes
  @Test
  def checkNumberOFNodes : Unit = {
    assertTrue(output.length == 4)
  }

  //Unit Test 5: Check that the Spark Dataframe functionality extracts correct number of publications based on the <article> tag
 @Test
  def checkDataFrame : Unit = {

   //Create a Spark Session object to interact with the Spark functionality
   val spark = SparkSession.builder.master(config.getString("Spark.master")).appName(config.getString("Spark.appName")) getOrCreate

   //Parse XML data and create a dataframe containing all publications with the <article> tag in the input file
   val articlesDf = spark.read.format("xml").option("rowTag", "article").load("publication.xml")

   //Check that the input file 'publication.xml' contains 9 publications with tag <article>
    assertTrue(articlesDf.count() == 9)

  }

  //Unit Test 6: Test whether UIC CS Professor values are read correctly
  @Test
  def uicProfTest : Unit = {

    val uicCsProfsFile = Source.fromFile("./src/main/resources/uic_cs_profs.txt")                                                   //Load the file uic_cs_profs.txt
    val lines = uicCsProfsFile.getLines()                                                                                           //Read the lines from the input CS UIC Professors file
    val uicCSProfsFile = lines.filter(f => !f.trim.isEmpty)
    val uicCsProfsMap = uicCSProfsFile.map(m2 => (m2.split(":")(0), m2.split(":")(1))).toMap                        //Generate map of UIC CS professors with key as names appearing in DBLP and value as names appearing in the UIC faculty site
    assertTrue(uicCsProfsMap("Tanya Y. Berger-Wolf") == "Tanya Berger-Wolf")
  }
  spark.stop()

}












