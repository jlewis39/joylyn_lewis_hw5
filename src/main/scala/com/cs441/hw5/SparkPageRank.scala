package com.cs441.hw5

//CS 441 HW5   : Implementing Spark computational model for calculating Page Rank values on the DBLP dataset
//Submitted by : Joylyn Lewis

//Description  : This program provides the implementation of the Spark computational model for calculating page rank values of nodes of the DBLP dataset.
// The nodes considered here are the UIC CS faculty professors and publication venues.

//Import required libraries
import java.io._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import collection.JavaConversions._
import com.typesafe.config._
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger

object SparkPageRank {

  def main(args: Array[String]): Unit = {

    try {

      val config = ConfigFactory.load("application.conf")                                              //Load the application's configuration from the classpath resource basename
      val logger = Logger(LoggerFactory.getLogger(config.getString("Spark.logger")))                             //Create a logger instance for supplying logging statements

      //Exit if command line arguments are not provided. Input should be provided in the form <input filename> <output filename> <number of iterations>
            if (args.length < 1) {
              logger.error("Please provide arguments: Input File Name, Output File Name and Number of Iterations")
              System.exit(1)
            }

      //Generate map of UIC CS Faculty Professors
      val uicCsProfsFile: InputStream = getClass.getResourceAsStream(config.getString("Spark.uicCsProfsFile"))   //Load the resource file 'uic_cs_profs.txt' which contains names of UIC CS Faculty members
      val linesProfs = scala.io.Source.fromInputStream(uicCsProfsFile).getLines()                                       //Read the lines from the input CS UIC Professors file
      val uicCSProfsFile = linesProfs.filter(f => !f.trim.isEmpty)
      val uicCsProfsMap = uicCSProfsFile.map(m2 => (m2.split(":")(0), m2.split(":")(1))).toMap           //Generate map of UIC CS professors with key as names appearing in DBLP and value as names appearing in the UIC faculty site

      logger.info("Generated map of UIC CS Faculty Professors")
      //Create a Spark Session object to interact with the Spark functionality
      //Comment below line if not running Spark application locally
      val spark = SparkSession.builder.master(config.getString("Spark.master")).appName(config.getString("Spark.appName")).getOrCreate

      //Below commented line is to be uncommented if running Spark application on cluster - Tried this for running on AWS EMR
      //val spark = SparkSession.builder.appName(config.getString("Spark.appName")).getOrCreate

      //Parse XML data and create a dataframe containing all publications with the <article> tag in the input file
      val articlesDf = spark.read.format(config.getString("Spark.xmlFormat")).option(config.getString("Spark.rowTag"), config.getString("Spark.articleTag")).load(args(0))

      //Select list of authors for each <article> publication and rename column as 'authorNew', also select value of <journal> tag and rename column as 'venue'
      val listOfArticlesDf = articlesDf.select(col(config.getString("Spark.authorTag"))(config.getString("Spark.valueTag")) as config.getString("Spark.authorNewTag"),
        col(config.getString("Spark.journalTag")) as config.getString("Spark.venueTag"))


      //Parse XML data and create a dataframe containing all publications with the <inproceedings> tag in the input file
      val inproceedingsDf = spark.read.format(config.getString("Spark.xmlFormat")).option(config.getString("Spark.rowTag"), config.getString("Spark.inproceedingsTag")).load(args(0))

      //Select list of authors for each <inproceedings> publication and rename column as 'authorNew', also select value of <booktitle> tag and rename column as 'venue'
      val listOfInproceedingsDf = inproceedingsDf.select(col(config.getString("Spark.authorTag"))(config.getString("Spark.valueTag")) as config.getString("Spark.authorNewTag"),
        col(config.getString("Spark.bookTitleTag")) as config.getString("Spark.venueTag"))


      //Parse XML data and create a dataframe containing all publications with the <proceedings> tag in the input file
      val proceedingsDf = spark.read.format(config.getString("Spark.xmlFormat")).option(config.getString("Spark.rowTag"), config.getString("Spark.proceedingsTag")).load(args(0))

      //Select list of authors for each <proceedings> publication and rename column as 'authorNew', also select value of <booktitle> tag and rename column as 'venue'
      val listOfProceedingsDf = proceedingsDf.select(col(config.getString("Spark.editorTag"))(config.getString("Spark.valueTag")) as config.getString("Spark.authorNewTag"),
       col(config.getString("Spark.bookTitleTag")) as config.getString("Spark.venueTag"))

      //Combine all three dataframes for <article>, <inproceedings> and <proceedings> into a single dataframe
      val combinedDf = listOfArticlesDf.union(listOfInproceedingsDf).union(listOfProceedingsDf)

      //Obtain number of iterations to be performed from input command line argument else default value will be used
      val iterations = if (args.length > 1) args(2).toInt else config.getString("Spark.defaultIterations").toInt

      //Declare an accumulator to collect the values, which starts with empty list and accumulates inputs by adding them into the list
      val accum = spark.sparkContext.collectionAccumulator[(String, String)]


      //For each publication, generate list of tuples representing an edge between the co-authors(UIC) of a publication and the publication venue
      combinedDf.foreach(f => {
        val authors = f.getAs[scala.collection.mutable.WrappedArray[String]](config.getString("Spark.authorNewTag"))          //Obtain the list of authors for current publication
        if (authors != null) {
          val listAuthors = authors.toList.filter(uicCsProfsMap.contains(_))                                                         //Obtain the UIC professors for current publication
          if (listAuthors.size != 0) {
            if (f.getAs[String](config.getString("Spark.venueTag")) != null) {                                                //Check that venue tag exists for the UIC professors mentioned for current publication
              val listAuthorsVenue = f.getAs[String](config.getString("Spark.venueTag")) :: listAuthors                       //Append the venue to the list of UIC professors for current publication

              //Generate list of tuples where a tuple is either a pair of UIC professors who worked on the publication or each UIC professor associated with the publication venue
              val listOfPairs = listAuthorsVenue.flatMap(x => listAuthorsVenue.map(y => (x, y))).filter(pair => pair._1 != pair._2)

              //Add each tuple created into the accumulator collection
              listOfPairs.foreach(pair => {
                accum.add(pair)
              })

            }

          }

        }

      })

      //Parallelize the collection accumulator value so that the elements form a RDD that can be operated on in parallel
      val lines = spark.sparkContext.parallelize(accum.value)

      //Apply the groupByKey command on the key/value pair RDD to produce the links RDD, which is also a key/value pair
      val links = lines.distinct().groupByKey().cache()

      //create the ranks <key,one> RDD from the links RDD. Here, initial seed number 1.0 is assigned
      var ranks = links.mapValues(v => config.getString("Spark.initialRank").toDouble)

      //Iterate the page rank algorithm as per required number of times
      1 to iterations foreach {_ =>
        {
          //calculate the contributions of the node, contrib = rank/size
          val contribs = links.join(ranks).values.flatMap { case (nodes, rank) =>
            val size = nodes.size
            nodes.map(node => (node, rank / size))
          }
          //Set each new rank for each node based on the formula: (1-d) + d * contrib, where d is the damping factor
          ranks = contribs.reduceByKey(_ + _).mapValues(config.getString("Spark.randomFactor").toDouble + config.getString("Spark.dampingFactor").toDouble * _)
        }
      }

      //To print the page rank output results, first fetch the entire RDD
      val output = ranks.collect()

      //Save the RDD into a single text file
      ranks.coalesce(1,true).saveAsTextFile(args(1))
      //Print the page rank values of each node
      output.foreach(tup => logger.info(tup._1 + " has rank: " + tup._2 + "."))

      //Stop the spark session
      spark.stop()
      logger.info("Spark session has been stopped")

    }
    catch {
      case e: Exception => e.printStackTrace
        val config = ConfigFactory.load("application.conf")                                            //Load the application's configuration from the classpath resource basename
        val logger = Logger(LoggerFactory.getLogger(config.getString("Spark.logger")))
        logger.error("The application has been terminated due to an unexpected error")                                  //Log error in case of exception
    }
  }

}
